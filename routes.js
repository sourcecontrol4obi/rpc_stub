const { isArray, filter } = require('lodash');

const RequestProcessor = require('./requestProcessor');

module.exports = function (types, methods) {
  return {
    // this is the rpc endpoint
    // every operation request will come through here
    '/rpc': async function (body) {
      let _json = undefined;
      try {
        if (!body) throw '';
        _json = JSON.parse(body);
      } catch (error) {
        return RequestProcessor.get2_0_error(null, -32700, 'Parse error');
      }

      let promiseArr = [];

      if (isArray(_json)) {
        if (_json.length < 1) return RequestProcessor.get2_0_error(null, -32600, 'Invalid request');
        for (req of _json) {
          promiseArr.push(RequestProcessor.jsonrpc2_0_request(types, methods, req).catch((err) => err));
        }
        let result = await Promise.all(promiseArr);
        let filtered = filter(result, (r) => r !== undefined);
        return filtered.length > 0 ? filtered : undefined;
      } else {
        try {
          return await RequestProcessor.jsonrpc2_0_request(types, methods, _json);
        } catch (error) {
          console.log(JSON.stringify(error));
          return error;
        }
      }
    },

    // this is our docs endpoint
    // through this the clients should know
    // what methods and datatypes are available
    '/describe': async function () {
      // load the type descriptions
      let type = {};
      let method = {};

      // set types
      type = types;

      //set methods
      for (let m in methods) {
        let _m = JSON.parse(JSON.stringify(methods[m]));
        method[m] = _m;
      }

      return {
        types: type,
        methods: method
      };
    }
  };
}