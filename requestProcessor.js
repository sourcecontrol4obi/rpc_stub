const Schema = require('validate');
const { map, forEach, flattenDeep, split, first, trim, isString } = require('lodash');

const Constants = require('./constants');
const ConstantTypes = map(Constants.Types, (t) => t.name);
const ConstantTypesMap = {};

// fill constant types map
forEach(Constants.Types, (t) => { ConstantTypesMap[t.name] = t });

const get2_0_error = (id, code, message, data) => {
  if (id === undefined) return undefined;
  return { jsonrpc: '2.0', error: { code, message, data }, id };
}

const get2_0_result = (id, data) => {
  if (id === undefined) return undefined;
  return { jsonrpc: '2.0', result: data, id };
}

const jsonrpc2_0_request = async (types, methods, object) => {
  let { method, params, id } = object;

  // validate request
  if (!isString(method)) throw get2_0_error(null, -32600, 'Invalid request');

  // turn params into array even if object
  params = flattenDeep([].concat(params));

  // version check
  if (object.jsonrpc !== '2.0') throw get2_0_error(id, -32600, "Invalid Request", { message: "Unsupported version" });

  // method availability check
  if (!methods[method]) throw get2_0_error(id, -32601, "Method not found");

  // parameter validity check
  let mParams = methods[method].params;
  if (mParams.length !== params.length) throw get2_0_error(id, -32602, "Invalid params", { message: `expects (${mParams})` });

  let errorMessage = undefined;

  let paramsBkp = JSON.parse(JSON.stringify(params));
  for (let i = 0; i < mParams.length; i++) {
    let param = mParams[i];
    let name = trim(first(split(param, ':')));
    let validator = undefined;
    // check constant types
    if (ConstantTypes.indexOf(name) >= 0) {
      validator = new Schema({ [param]: { required: true, type: ConstantTypesMap[name] } }, { typecast: true });
      validator.typecaster('Object', val => val);
      errorMessage = validator.validate({ [param]: paramsBkp[i] });
    } else if (types[name]) {
      validator = new Schema(types[name].schema, { typecast: true });
      errorMessage = validator.validate(paramsBkp[i]);
    }

    if (errorMessage && errorMessage.length > 0) {
      let errorData = {
        path: errorMessage[0].path,
        message: errorMessage[0].message
      }
      throw get2_0_error(id, -32602, "Invalid params", errorData);
    }
  }

  // perform response handling and validation
  // TODO: refactor response to comply with spec
  try {
    let result = await methods[method].exec.apply(null, params);
    return get2_0_result(id, result);
  } catch (error) {
    throw get2_0_error(id, -32603, error.message || "Internal error", error);
  }
}

module.exports = {
  get2_0_result,
  get2_0_error,
  jsonrpc2_0_request
}