let http = require('http');

let Handler = require('../index');
let methods = require('./methods');
let types = require('./types');

let handler = new Handler(types, methods, async (req) => {
  try {
    console.log(req.headers);
    let auth = req.headers['authorization'];
    console.log(auth);
    if (auth === 'test') return true;
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
});
let server = http.createServer(handler.getListener());
const PORT = process.env.PORT || 9090;

server.listen(PORT);
console.log('listening on port', PORT);
