let { filter } = require('lodash');

let users = [
  { id: 1, name: 'john snow', age: 25, sex: 'male' },
  { id: 2, name: 'bryan adams', age: 35, sex: 'male' },
  { id: 3, name: 'stacy miles', age: 18, sex: 'female' },
  { id: 4, name: 'ruppet grint', age: 18, sex: 'male' },
  { id: 5, name: 'emma watson', age: 16, sex: 'female' },
  { id: 6, name: 'daniel radcliff', age: 18, sex: 'male' },
]

module.exports = {
  getUsers: {
    description: 'returns the users that match the query',
    params: ['User'],
    returns: ['Array<User>'],
    exec: async (data) => {
      console.log('params', data);
      let result = filter(users, data);
      console.log('result', result);
      return result;
    }
  },
  max: {
    description: 'returns the max between two numbers',
    params: ['Number: operand one', 'Number: operand 2'],
    exec: async (one, two) => {
      console.log(one, two);
      return one > two ? one : two;
    }
  }
}