module.exports = {
  User: {
    description: 'user object',
    schema: {
      id: { type: Number },
      name: { type: String },
      age: { type: Number, required: true },
      sex: { type: String },
      dob: { type: Date }
    }
  }
}