### A node based JSON-RPC 2.0 server stub
[JSON-RPC specification](https://www.jsonrpc.org/specification)
- Uses node http currently to handle requests
- Includes method params validation
- Take a look at the example folder for implementation guide

### Type Structure
```js
{
  TypeName: {
    description: 'description of type',
    schema: {
      prop1: { type: Number },
      prop2: { type: String },
      ...
    }
  },
  ...
}
```
- TypeName should be capitalized
- schema follows structure from [validate](https://www.npmjs.com/package/validate)

### Method Structure
```
{
  methodName: {
    description: 'method description',
    params: ['Type1',...],
    returns: ['Type2',...],
    exec: async (data) => {
      ...
    }
  },
  ...
}
```
- Types can be defined as in [validate](https://www.npmjs.com/package/validate)
- Types can as well be primitives such as 
```js
Number, String, Boolean, Array, Object
```
- TODO: treat array types with  
```js
Array<Type>
```

### Usage
```js
let http = require('http');

let RPC = require('rpc_stub');
// your methods
let methods = require('./methods');
// your types
let types = require('./types');
// authenticate
let authenticate = async (req) => {
  // request authentication code
}

let handler = new RPC(types, methods, authenticate); 
let server = http.createServer(handler.getListener());
const PORT = process.env.PORT || 9090;

server.listen(PORT);
console.log('listening on port',PORT);
```

### TODO
- [x] Add parameter validation
- [ ] Add Unit Tests
- [ ] Add Response Validation
- [x] Add access control