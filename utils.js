const Constants = require('./constants');

module.exports = {
  jsonReplacer: function (key, value) {
    if (key === 'type' && Constants.Types.indexOf(value) >= 0) {
      return value.name;
    }

    return value;
  }
}