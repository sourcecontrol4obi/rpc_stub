'use strict';

const url = require('url');

const Routes = require('./routes');
const Utils = require('./utils');

module.exports = class Handler {
  constructor(types, methods, authenticate) {
    this.authenticate = authenticate;
    this.routes = Routes(types, methods);
  }

  // request Listener
  // this is what we'll feed into http.createServer
  async _listener(request, response) {
    let routes = this.routes;
    let reqUrl = `http://${request.headers.host}${request.url}`;
    let parseUrl = url.parse(reqUrl, true);
    let pathname = parseUrl.pathname;

    // we're doing everything json
    response.setHeader('Content-Type', 'application/json');

    // buffer for incoming data
    let buf = null;

    if (this.authenticate && !await this.authenticate(request)) {
      response.statusCode = 401;
      response.end(JSON.stringify({ message: 'authentication failed' }));
      return;
    }

    // listen for incoming data
    request.on('data', data => {
      if (buf === null) {
        buf = data;
      } else {
        buf = buf + data;
      }
    });

    // on end proceed with compute
    request.on('end', async () => {
      let body = buf !== null ? buf.toString() : null;

      if (routes[pathname]) {
        let compute = routes[pathname].call(null, body);

        try {
          let res = await compute;
          response.end(JSON.stringify(res, Utils.jsonReplacer));
        } catch (error) {
          console.error(error);
          response.statusCode = 500;
          response.end(JSON.stringify(error));
        }

      } else {
        response.statusCode = 404;
        response.end({ message: `oops! ${pathname} not found here` })
      }
    })
  }

  getListener() {
    return this._listener.bind(this);
  }
}
